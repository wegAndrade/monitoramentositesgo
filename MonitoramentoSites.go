package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

const monitoramentos = 1
const delay = 10000

func main() {
	for {
		exibeMenu()
		comando := lerComando()
		executaComando(comando)
		println(" ")
	}
}

func exibeMenu() {
	versao := "1.1.1"
	println("Programa na versão: ", versao)
	println("1 - Iniciar Monitoramento.")
	println("2 - Exibir logs")
	println("3 - Registrar novo site")
	println("0 - Sair do programa")
}

func lerComando() int {
	var comandoLido int
	fmt.Scan(&comandoLido)
	return comandoLido

}
func executaComando(comando int) {
	switch comando {
	case 0:
		println("Encerrando... ")
		os.Exit(0)
	case 1:
		println("Iniciando monitoramento... ")
		iniciarMonitoramento()
	case 2:
		println("Exibindo logs... ")
		exibeLog()
		println("Fim dos logs... ")
	// case 3:
	// 	println("Registrando novo site ... ")
	// 	registrarSite()
	// 	println("Fim do registro do novo site... ")
	default:
		println("Não conheço este comando!")
		os.Exit(-1)
	}
}
func iniciarMonitoramento() {
	println("Monitorando... ")
	sites := lerSitesDoArquivo()

	for i := 0; i < monitoramentos; i++ {
		println("Monitoração:", i)
		for i, site := range sites {
			println("Executando verificação no site", site, "posição", i, "\n")
			if site == "" {
				println("Não existem sites cadastrados para monitoramento")
				return
			}

			testaSite(site)
			println("")
		}
		println("")
		time.Sleep(delay * time.Second)
	}

	println("Finalizado monitoramento... ")
}
func testaSite(site string) {
	resp, err := http.Get(site)

	if err != nil {
		println("Ocorreu um erro", err.Error())
	}
	if resp.StatusCode == 200 {
		mensagem := fmt.Sprint("Site: ", site, " foi carregado com sucesso!")
		println(mensagem)
		registraLog(mensagem, "Sucess")
	} else {
		mensagem := fmt.Sprint("Site:", site, " esta com problemas. Status:", resp.Status)
		println(mensagem)
		registraLog(mensagem, "Error")
	}
	println("")
}
func lerSitesDoArquivo() []string {
	var sites = []string{}
	var arquivoCaminho string = "sites.txt"
	println("Abrindo arquivo", arquivoCaminho)
	arquivoEmBytes, err := os.Open(arquivoCaminho)
	if err != nil {
		mensagem := fmt.Sprint("Ocorreu um erro: ", err.Error())
		println(mensagem)
		registraLog(mensagem, "Error")
	}

	leitor := bufio.NewReader(arquivoEmBytes)
	for {
		linha, err := leitor.ReadString('\n')
		linha = strings.TrimSpace(linha)
		sites = append(sites, linha)
		if err == io.EOF {
			mensagem := fmt.Sprint("linhas do arquivo de sites lidas ")
			println(mensagem)
			registraLog(mensagem, "Info")
			break
		}
	}
	println("Fechando arquivo", arquivoCaminho)
	arquivoEmBytes.Close()
	return sites
}
func exibeLog() {
	arquivo, err := ioutil.ReadFile("log.txt")
	if err != nil {
		mensagem := fmt.Sprint("Erro ao ler  arquivo de logs! ", err.Error())
		println(mensagem)
		registraLog(mensagem, "Erro")
	}
	println("\n")
	println(string(arquivo))
}
func registraLog(mensagem string, logLevel string) {
	arquivo, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		println("Ocorreu um erro: ", err.Error())
	}
	println("Arquivo de log ", arquivo.Name())
	dataAtual := time.Now()
	arquivo.WriteString(dataAtual.Format("02/01/2006 15:04:05") + " - " + logLevel + ": " + mensagem + "\n")
	arquivo.Close()
}

// func registrarSite() {
// 	println("Digite o novo site  a ser adicionado na lista de monitoramento: ")
// 	var sitelido string
// 	fmt.Scan(&sitelido)
// 	arquivo, err := os.OpenFile("sites.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
// 	if err != nil {
// 		println("Ocorreu um erro: ", err.Error())
// 	}
// 	registraLog(fmt.Sprint("Salvando site:", sitelido), "Info")
// 	_, err = arquivo.WriteString("\r\n" + sitelido)
// 	if err != nil {
// 		mensagem := fmt.Sprint("Erro ao gravar novo site no arquivo de sites.txt! ", err.Error())
// 		println(mensagem)
// 		registraLog(mensagem, "Erro")
// 	}
// 	arquivo.Close()
// }
